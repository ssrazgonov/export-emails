<?php

use Illuminate\Support\Facades\Route;

use Goutte\Client;
use \Symfony\Component\DomCrawler\Crawler;

Route::get('/', function () {

    $client = new Client();

    $crawler = $client->request('GET', 'https://lipetsk.jsprav.ru');

    $json = [];

    $mainLevel = 2;
    $level = 10;

    $crawler
        ->filter('.categories .cats-list h3 a')
        ->each(function (Crawler $node)
          use(&$json, $client, &$level, &$mainLevel) {

        if ( $mainLevel <= 0 ) return;

//      Main category
        $category = [
            'name' => $node->text(),
            'type' => 'category',
            'children' => []
        ];

        $categoryCrawler = $client->click($node->link());

        $categoryCrawler
            ->filter('.cat-list a')
            ->each(function ($node)
              use($client, &$level, &$category) {

            if ($level <= 0 ) return;

            $subcategory = [
                'name' => preg_replace('/[^\\/\-a-zА-Яа-я,\s]/iu', '', $node->text()),
                'type' => 'category',
                'children' => []
            ];

            $subCategoryCrawler = $client->click($node->link());

            $subCategoryCrawler->filter('.ords-list .org')->each(function ($node) use (&$subcategory) {
                $subcategory['children'][] = [
                    'name' => $node->filter('h3 a')->text(),
                    'type' => 'org',
                    'attrs' => [
                        'email' => 'email',
                        'phones' => '',
                        'site' => ''
                    ]
                ];
            });

            $category['children'][] = $subcategory;

            $level--;

          sleep(1);
        });

        $json[] = $category;

        $mainLevel--;

//        sleep(1);

    });


    Storage::disk('public')->put('catalog.json', json_encode($json, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE ));

    return view('welcome')->with(['json' => $json]);
});
