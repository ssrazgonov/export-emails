<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    </head>
    <body>

    <ul>
    @foreach($json as $item)
        <li>{{$item['name']}}
            @if(!empty($item['children']))
            <ul>
                @foreach($item['children'] as $subItem)
                    <li>
                        {{$subItem['name']}}
                        @if(!empty($subItem['children']))
                            <ul>
                                @foreach($subItem['children'] as $org)
                                    <li>
                                        {{$org['name']}} {{$org['attrs']['email']}}
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
            @endif
        </li>
    @endforeach
    </ul>
    </body>
</html>
