<?php

namespace App\Console\Commands;

use App\Mail\SendMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Mailgun\Mailgun;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $json_list = json_decode(Storage::get('catalog.json'));
        $emails = [];
        $count = 0;
        $errors = 0;

        collect($json_list)->each(function ($row) use (&$emails) {
            collect($row->children)->each(function ($row) use (&$emails) {
                collect($row->children)->each(function ($org) use (&$emails) {
                    $emails[] = ['name' => $org->name, 'email' => $org->attrs->email];
                });
            });
        });

        $bar = $this->output->createProgressBar(count($emails));

        foreach ($emails as $email) {

            try {
                Mail::to($email['email'])->send(new SendMail($email['name']));
                $count++;
                Log::debug('отправлено - ' . $count. ' писем');
            } catch (\Exception $exception) {
                $this->error('ошибка отправки на почту' . $email['email']);
                Log::error('ошибка отправки на почту' . $email['email']);
                $errors++;
            }

            $bar->advance();

            sleep(60);
        }

        Log::debug('ВСЕГО отправлено - ' . $count. ' писем');
        Log::debug('ошибки отправки - ' . $errors. ' писем');

        return 0;
    }
}
