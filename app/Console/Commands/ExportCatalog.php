<?php

namespace App\Console\Commands;

use Goutte\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;

class ExportCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ExportCatalog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $total = 12000;
        $current = 0;

        $bar = $this->output->createProgressBar(12000);

        $client = new Client();

        $crawler = $client->request('GET', 'https://lipetsk.jsprav.ru');

        $json = [];

        $mainLevel = 999;
        $level = 999999;

        $crawler
            ->filter('.categories .cats-list h3 a')
            ->each(function (Crawler $node)
            use(&$json, $client, &$level, &$mainLevel, &$bar) {

                if ( $mainLevel <= 0 ) return;

//      Main category
                $category = [
                    'name' => $node->text(),
                    'type' => 'category',
                    'children' => []
                ];

                $categoryCrawler = $client->click($node->link());

                $categoryCrawler
                    ->filter('.cat-list a')
                    ->each(function ($node)
                    use($client, &$level, &$category, &$bar) {

                        if ($level <= 0 ) return;

                        $subcategory = [
                            'name' => preg_replace('/[^\\/\-a-zА-Яа-я,\s]/iu', '', $node->text()),
                            'type' => 'category',
                            'children' => []
                        ];

                        $subCategoryCrawler = $client->click($node->link());

                        $subCategoryCrawler->filter('.ords-list .org')->each(function ($node) use (&$subcategory, &$bar) {
                            $emailNode = $node->filter('.address span.email');

                            $email = $emailNode->count() == 0 ?  '' : $emailNode->first()->text();

                            if ($email == '') {
                                return;
                            }

                            $subcategory['children'][] = [
                                'name' => $node->filter('h3 a')->text(),
                                'type' => 'org',
                                'attrs' => [
                                    'email' => $email,
                                ]
                            ];
                        });

                        $category['children'][] = $subcategory;

                        $level--;

                        $bar->advance();

                        usleep(100000);
                    });

                $json[] = $category;

                $mainLevel--;

//        sleep(1);

            });


        Storage::disk('public')->put('catalog.json', json_encode($json, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE ));
        return 0;
    }
}
